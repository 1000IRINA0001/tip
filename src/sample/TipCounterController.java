package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class TipCounterController {
    private static final String DATA_ENTRY_ERROR_MESSAGE = "Введены некоректные данные";
    private static final String NO_DATA_ENTERED = "Введите сумму чека";

    private static final double DOLLAR = 73.56;
    private static final double EURO = 79.48;

    @FXML
    private TextField billAmount;

    @FXML
    private ComboBox<?> currencyComboBox;

    @FXML
    private RadioButton noTip;

    @FXML
    private ToggleGroup tipGroup;

    @FXML
    private RadioButton fivePercentTip;

    @FXML
    private RadioButton tenPercentTip;

    @FXML
    private Button calkulateAmount;

    @FXML
    private CheckBox roundUp;

    @FXML
    private CheckBox countRubles;

    @FXML
    private TextField tipCount;

    @FXML
    private TextField billCount;

    @FXML
    private Pane pane;

    @FXML
    private TextField tipCountRubles;

    @FXML
    private TextField dataEntryError;

    @FXML
    private TextField noDataEntered;

    @FXML
    private TextField billCountRubles;


    //метод очистки
    @FXML
    void clear(ActionEvent event) {
        billAmount.clear();
        roundUp.setSelected(false);
        countRubles.setSelected(false);
        tipCount.clear();
        tipCountRubles.clear();
        billCount.clear();
        billCountRubles.clear();
    }

    @FXML
    void count(ActionEvent event) {
        try {
            countTip();
            countBill();
            countBillInRubles();
            roundCountBill();
            panelAppearance();
            noDataEntered.setVisible(false);
            dataEntryError.setVisible(false);
        } catch (Exception e) {
            someData();

        }
    }

    @FXML
    void initialize() {
    }

    /**
     * метод выбора радиокнопки
     *
     * @return выбранную радиокнопку
     */
    private RadioButton selectionTip() {
        Toggle choice = tipGroup.getSelectedToggle();
        if (choice == noTip) {
            return noTip;
        }
        if (choice == fivePercentTip) {
            return fivePercentTip;
        }
        if (choice == tenPercentTip) {
            return tenPercentTip;
        }
        return null;
    }

    /**
     * метод подсчета суммы чаевых
     *
     * @return сумму чаевых
     */
    private double countTip() {

        double amountBill = parse(billAmount);
        double countTip = 0;
        if (selectionTip() == noTip) {
            countTip = 0;
            tipCount.setText(String.valueOf(countTip));
        }
        if (selectionTip() == fivePercentTip) {
            countTip = amountBill * 0.05;
            tipCount.setText(String.valueOf(countTip));
        }
        if (selectionTip() == tenPercentTip) {
            countTip = amountBill * 0.1;
            tipCount.setText(String.valueOf(countTip));
        }

        return countTip;
    }

    /**
     * метод подсчитывает чек к оплате
     */
    private double countBill() {
        double countBill = 0;
        double tip = countTip();
        double amountBill = parse(billAmount);
        countBill = amountBill + tip;

        billCount.setText(String.valueOf(countBill));

        return countBill;
    }

    /**
     * метод выбора валюты в комбобоксе
     *
     * @return выбранную валюту
     */
    private int currencySelected() {
        if ("Рубли".equals(currencyComboBox.getValue())) {
            return 1;
        }
        if ("Доллары".equals(currencyComboBox.getValue())) {
            return 2;
        }
        if ("Евро".equals(currencyComboBox.getValue())) {
            return 3;
        }
        return 0;
    }

    /**
     * метод перевода суммы чаевых и чека из иностранной валюты в рубли
     */
    private void countBillInRubles() {
        double countBill = countBill();
        double countTip = countTip();
        if (someData() == 2) {
            tipCount.setText(null);
            billCount.setText(null);
        } else {
            if (currencySelected() == 2) {
                countTip = countTip * DOLLAR;
                countBill = countBill * DOLLAR;
            }
            if (currencySelected() == 3) {
                countTip = countTip * EURO;
                countBill = countBill * EURO;
            }
            billCountRubles.setText(String.valueOf(countBill));
            tipCountRubles.setText(String.valueOf(countTip));
        }
    }

    /**
     * метод форматирует число до 2 знаков после запятой(2,23)
     *
     * @param bill число, которое форматируется
     */
    private void format(TextField bill) {
        double number = Double.parseDouble(bill.getText());
        DecimalFormat df = new DecimalFormat("0.00");
        String str;
        df.setRoundingMode(RoundingMode.HALF_UP);
        str = df.format(number);
        bill.setText(str);
    }

    /**
     * метод , в котором прописано, какие величины отформатируются
     * (чаевые , чаевые в рублях, к оплате, к оплате в рублях)
     */
    private void formatPayment() {
        format(tipCount);
        format(tipCountRubles);
        format(billCount);
        format(billCountRubles);
    }

    /**
     * метод округляет поля (к оплате, к оплате в рублях)
     */
    private void roundCountBill() {
        if (roundUp.isSelected() == true) {
            round(billCount);
            round(billCountRubles);
        }
        formatPayment();
    }

    /**
     * метод округления числа
     * если без чаевых, округляет в большую сторону
     * если с чавыми - в меньшую
     *
     * @param bill число, которое нужно округлить
     */
    private void round(TextField bill) {
        double number;
        double countBill = Double.parseDouble(bill.getText());
        if (currencyComboBox.equals("Рубли")) {
            number = Math.ceil(countBill);
        } else {
            number = Math.floor(countBill);
        }
        bill.setText(String.valueOf(number));
    }

    /**
     * метод, показывающий панель после нажатия checkBox "Посчитать в рублях"
     */
    private void panelAppearance() {

        if (countRubles.isSelected() == true) {
            if (currencySelected()==1) {
                pane.setVisible(false);
            }else {
                pane.setVisible(true);
            }
        }else{
            pane.setVisible(false);
        }
    }


    double parse(TextField bill) {
        return Double.parseDouble(bill.getText());
    }

    boolean validation() {
        try {
            parse(billAmount);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * vtтод проверяющий поле "сумма чека" на введенные данные
     *
     * @return возвращает номер ошибки(если нашел ее)
     */
    private int someData() {
        dataEntryError.setVisible(false);
        noDataEntered.setVisible(false);
        double number = Double.parseDouble(String.valueOf(billAmount.getText()));
        if (billAmount.getText().equals("")) {
            noDataEntered.setVisible(true);
            noDataEntered.setText(NO_DATA_ENTERED);
            return 1;
        }

        if (validation() == false || number <= 0) {
            dataEntryError.setVisible(true);
            dataEntryError.setText(DATA_ENTRY_ERROR_MESSAGE);
            return 2;
        }
        return 0;
    }
}






