package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("tipCounter.fxml"));
      //  primaryStage.getIcons().add(new Image("sample\\assets\\ .png"));
        primaryStage.setTitle("Приложение для подчета суммы чека с чаевыми");
        primaryStage.setScene(new Scene(root, 328, 503));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
